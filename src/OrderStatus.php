<?php

namespace Bittacora\OrderStatus;

use Bittacora\OrderStatus\Models\OrderStatusModel;

class OrderStatus
{
    public function getAllAsArray(){
        return OrderStatusModel::orderBy('order_column', 'ASC')->pluck('name', 'id')->toArray();
    }
}
