<?php

namespace Bittacora\OrderStatus\Models;

use Bittacora\Bpanel4Users\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Translatable\HasTranslations;
use Wildside\Userstamps\Userstamps;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * \Bittacora\OrderStatus\Models\OrderStatusModel
 *
 * @method static self whereId(int $id)
 * @method self firstOrFail()
 * @method static whereRaw(string $string)
 * @property int $id
 * @property array $name
 * @property array|null $annotation
 * @property string|null $color
 * @property string|null $background_color
 * @property int|null $order_column
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int $active
 * @property int $default
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User|null $creator
 * @property-read User|null $destroyer
 * @property-read User|null $editor
 * @method static Builder|OrderStatusModel newModelQuery()
 * @method static Builder|OrderStatusModel newQuery()
 * @method static Builder|OrderStatusModel ordered(string $direction = 'asc')
 * @method static Builder|OrderStatusModel query()
 * @method static Builder|OrderStatusModel whereActive($value)
 * @method static Builder|OrderStatusModel whereAnnotation($value)
 * @method static Builder|OrderStatusModel whereBackgroundColor($value)
 * @method static Builder|OrderStatusModel whereColor($value)
 * @method static Builder|OrderStatusModel whereCreatedAt($value)
 * @method static Builder|OrderStatusModel whereCreatedBy($value)
 * @method static Builder|OrderStatusModel whereDefault($value)
 * @method static Builder|OrderStatusModel whereName($value)
 * @method static Builder|OrderStatusModel whereOrderColumn($value)
 * @method static Builder|OrderStatusModel whereUpdatedAt($value)
 * @method static Builder|OrderStatusModel whereUpdatedBy($value)
 * @mixin Eloquent
 */
class OrderStatusModel extends Model implements Sortable
{
    use HasFactory;
    use HasTranslations;
    use SortableTrait;
    use Userstamps;
    use SoftDeletes;

    public $sortable = [
        'sort_when_creating' => true,
    ];

    protected $fillable = [
        'name',
        'annotation',
        'color',
        'background_color',
        'order_column',
        'updated_by',
        'created_by',
        'active',
        'default',
    ];

    public $translatable = [
        'name',
        'annotation',
    ];

    protected $table = 'order_status';

    public static function getByName(string $name, string $lang = 'es'): self
    {
        return self::whereRaw("JSON_EXTRACT(name, '$.{$lang}') = '{$name}'")->firstOrFail();
    }
}
