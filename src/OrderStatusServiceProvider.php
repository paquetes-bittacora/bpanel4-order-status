<?php

namespace Bittacora\OrderStatus;

use Bittacora\OrderStatus\Http\Livewire\OrderStatusDatatable;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\OrderStatus\Commands\OrderStatusCommand;

class OrderStatusServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('order-status')
            ->hasConfigFile()
            ->hasViews()
            ->hasMigration('create_order_status_table')
            ->hasCommand(OrderStatusCommand::class);
    }

    public function register()
    {
        $this->app->bind('order-status', function($app){
            return new OrderStatus();
        });
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'order-status');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'order-status');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        if ($this->app->runningInConsole()) {
            $this->commands([
                OrderStatusCommand::class
            ]);
        }

        \Livewire::component('order-status::order-status-datatable', OrderStatusDatatable::class);
    }
}
