<?php

namespace Bittacora\OrderStatus\Http\Livewire;

use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class OrderStatusDatatable extends DataTableComponent
{

    public int $contentId;
    public bool $reordering = true;
    public string $reorderingMethod = 'reorder';
    public bool $showPerPage = false;
    public bool $showPagination = true;
    public bool $showSearch = false;
    public array $perPageAccepted = [10,25,50,100];
    public string $emptyMessage = "El registro de estados de pedido está vacío";


    public function columns(): array
    {
        return [
            Column::make('Título', 'title')->addClass('w-30'),
            Column::make('Idiomas')->addClass('w-20 text-center'),
            Column::make('Activo', 'active')->addClass('w-15 text-center'),
            Column::make('Orden', 'order_column')->addClass('w-15 text-center'),
            Column::make('Acciones')->addClass('w-10')
        ];
    }

    public function query()
    {
        return OrderStatusModel::query()->orderBy('order_column', 'ASC');
    }

    public function rowView(): string
    {
        return 'order-status::livewire.order-status-datatable';
    }

    public function reorder($list){
        foreach($list as $item){
            OrderStatusModel::where('id', $item['value'])->update(['order_column' => $item['order']]);
        }
    }

    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar'
        ];
    }

    public function bulkDelete(){
        if(count($this->selectedKeys())){
            foreach($this->selectedKeys() as $key){
                $page = OrderStatusModel::where('id', $key) -> first();
                $page->delete();
            }
            $ids = OrderStatusModel::ordered()->pluck('id');
            OrderStatusModel::setNewOrder($ids);
            $this->resetAll();
        }
    }
}
