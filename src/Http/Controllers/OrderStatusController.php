<?php

namespace Bittacora\OrderStatus\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Language\LanguageFacade;
use Bittacora\OrderStatus\Http\Requests\StoreOrderStatusRequest;
use Bittacora\OrderStatus\Http\Requests\UpdateOrderStatusRequest;
use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OrderStatusController extends Controller
{
    public function index()
    {
        $this->authorize('order-status.index');
        return view('order-status::index');
    }

    public function create($language = null)
    {
        $this->authorize('order-status.create');
        if (empty($language)) {
            $defaultLanguage = LanguageFacade::getDefault();
            $language = $defaultLanguage->locale;
        }
        return view('order-status::create', ['language' => $language]);
    }

    public function store(StoreOrderStatusRequest $request)
    {
        $this->authorize('order-status.create');
        $model = new OrderStatusModel();
        $model->setLocale($request->input('locale'));

        $model->fill($request->all());
        $model->save();

        $locale = $request->input('locale');
        return redirect()->route('order-status.edit', ['model' => $model, 'locale' => $locale])->with(['alert-success' => __('order-status::order-status.created')]);
    }

    public function edit(OrderStatusModel $model, $language = null)
    {
        $this->authorize('order-status.edit');
        if (empty($language)) {
            $language = LanguageFacade::getDefault()->locale;
        }

        $model->setLocale($language);
        return view('order-status::edit', ['language' => $language, 'model' => $model]);
    }

    public function update(UpdateOrderStatusRequest $request, OrderStatusModel $model, $locale)
    {
        $this->authorize('order-status.edit');
        $model->setLocale($request->input('locale'));
        $model->fill($request->all());
        $model->save();
        return redirect()->route('order-status.edit', ['model' => $model, 'locale' => $locale])->with(['alert-success' => __('order-status::order-status.updated')]);
    }

    public function destroy(OrderStatusModel $model)
    {
        $this->authorize('order-status.destroy');

        if ($model->delete()) {
            // Reordeno después de borrar
            $ids = OrderStatusModel::ordered()->pluck('id');
            OrderStatusModel::setNewOrder($ids);

            Session::put('alert-success', __('order-status::order-status.deleted'));
        } else {
            Session::put('alert-warning', __('order-status::order-status.not-deleted'));
        }
        return redirect()->route('order-status.index');
    }
}
