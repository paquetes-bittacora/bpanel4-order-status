<?php

namespace Bittacora\OrderStatus\Commands;

use Bittacora\AdminMenu\AdminMenuFacade;
use Bittacora\OrderStatus\Database\Seeders\OrderStatusSeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class OrderStatusCommand extends Command
{
    public $signature = 'order-status:install';

    public $description = 'My command';

    public function handle()
    {
        Artisan::call('db:seed', [
            '--class' => OrderStatusSeeder::class,
            '--force' => true,
        ]);

        $this->comment('Registrando elementos del menú...');
        $module = AdminMenuFacade::createModule('ecommerce', 'order-status', 'Estados de pedido', 'far fa-bells');
        AdminMenuFacade::createAction($module->key, 'Listar', 'index', 'fa fa-bars');
        AdminMenuFacade::createAction($module->key, 'Añadir', 'create', 'fa fa-plus');

        $this->comment('Hecho');

        $this->comment('Dando permisos al administrador...');
        $permissions = ['index', 'create', 'edit', 'destroy'];


        $adminRole = Role::findOrCreate('admin');
        foreach ($permissions as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'order-status.'.$permission]);
            $adminRole->givePermissionTo($permission);
        }

        $this->comment('Hecho');
    }
}
