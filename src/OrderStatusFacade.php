<?php

namespace Bittacora\OrderStatus;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\OrderStatus\OrderStatus
 */
class OrderStatusFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'order-status';
    }
}
