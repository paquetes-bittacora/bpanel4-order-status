<?php

use Bittacora\OrderStatus\Http\Controllers\OrderStatusController;

Route::prefix('bpanel')->middleware(['web', 'auth','admin-menu'])->name('order-status.')->group(function(){
    Route::get('order-status', [OrderStatusController::class, 'index'])->name('index');
    Route::get('order-status/create/language/{locale?}', [OrderStatusController::class, 'create'])->name('create');
    Route::post('order-status/store', [OrderStatusController::class, 'store'])->name('store');
    Route::get('order-status/{model}/edit/language/{locale?}', [OrderStatusController::class, 'edit'])->name('edit');
    Route::put('order-status/{model}/update/language/{locale?}', [OrderStatusController::class, 'update'])->name('update');
    Route::delete('order-status/{model}', [OrderStatusController::class, 'destroy'])->name('destroy');
});
