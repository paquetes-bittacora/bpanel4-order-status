<?php

namespace Bittacora\OrderStatus\Database\Factories;

use Bittacora\OrderStatus\Models\OrderStatusModel;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<OrderStatusModel>
 */
class OrderStatusModelFactory extends Factory
{
    protected $model = OrderStatusModel::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
        ];
    }
}
