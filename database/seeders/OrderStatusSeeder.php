<?php

namespace Bittacora\OrderStatus\Database\Seeders;

use Bittacora\Tabs\Tabs;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tabs::createItem('order-status.index', 'order-status.index', 'order-status.index', 'Estados de pedido','fa fa-list');
        Tabs::createItem('order-status.index', 'order-status.create', 'order-status.create', 'Nuevo estado','fa fa-plus');

        Tabs::createItem('order-status.create', 'order-status.index', 'order-status.index', 'Estados de pedido','fa fa-list');
        Tabs::createItem('order-status.create', 'order-status.create', 'order-status.create', 'Nuevo estado','fa fa-plus');

        Tabs::createItem('order-status.edit', 'order-status.index', 'order-status.index', 'Estados de pedido','fa fa-list');
        Tabs::createItem('order-status.edit', 'order-status.create', 'order-status.create', 'Nuevo estado','fa fa-plus');
    }
}
