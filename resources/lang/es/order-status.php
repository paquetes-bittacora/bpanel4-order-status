<?php

return [
    'order-status_list' => 'Listado de estados de pedido',
    'name' => 'Nombre',
    'annotation' => 'Anotación',
    'active' => 'Activo',
    'default' => 'Estado por defecto para pedidos aún no terminados de procesar',
    'create_default_language' => 'Al crear un nuevo estado, se creará en el idioma principal. Si desea añadir traducciones, guarde primero el estado de pedido en el idioma principal.',
    'add' => 'Añadir estado',
    'text-color' => 'Color de texto',
    'background-color' => 'Color de fondo',
    'created' => 'El estado de pedido ha sido creado correctamente',
    'updated' => 'El estado de pedido ha sido actualizado correctamente',
    'deleted' => 'El estado de pedido ha sido eliminado correctamente',
    'not-deleted' => 'El estado no pudo ser eliminado',
    'edit' => 'Editar estado de pedido',
    'index' => 'Listado de estados de pedido',
    'create' => 'Crear estado de pedido',
    'destroy' => 'Eliminar estado de pedido'
];
