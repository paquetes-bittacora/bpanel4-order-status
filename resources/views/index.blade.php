@extends('bpanel/layouts.bpanel-app')

@section('title', 'Estados de pedido')

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('order-status::order-status.order-status_list') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('order-status::order-status-datatable', [], key('orderstatusdatatable'))
        </div>

    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/livewire-sortablejs@0.x.x/dist/livewire-sortable.min.js" defer></script>
@endpush
