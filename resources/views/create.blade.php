@extends('bpanel/layouts.bpanel-app')

@section('title', 'Crear estado de pedido')

@section('content')
    <p class="alert alert-success">
        <i class="fas fa-info-circle"></i> {!!  __('order-status::order-status.create_default_language') !!}
    </p>
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0 d-flex justify-content-between">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('order-status::order-status.add') }}</span>
            </h4>
            <img src="{{asset('assets_bpanel/flags/'.$language.'.png')}}" alt="{{ $language }}">
        </div>

        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('order-status.store')}}" enctype="multipart/form-data">
            @csrf
            @livewire('form.input-text', ['name' => 'name', 'labelText' => __('order-status::order-status.name'), 'required'=>true])
            @livewire('utils::tinymce-editor', ['name' => 'annotation', 'labelText' => __('order-status::order-status.annotation')])
            @livewire('form.input-checkbox', ['name' => 'active', 'value' => 1, 'checked' => true, 'labelText' => __('order-status::order-status.active'), 'bpanelForm' => true])
            @livewire('form.input-checkbox', ['name' => 'default', 'value' => 1, 'checked' => false, 'labelText' => __('order-status::order-status.default'), 'bpanelForm' => true])
            @livewire('form.input-color', ['name' => 'color', 'labelText' => __('order-status::order-status.text-color'), 'fieldWidth' => 1])
            @livewire('form.input-color', ['name' => 'background_color', 'labelText' => __('order-status::order-status.background-color'), 'fieldWidth' => 1])
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form.save-button',['theme'=>'save'])
                @livewire('form.save-button',['theme'=>'reset'])
            </div>
            @livewire('form.input-hidden', ['name' => 'locale', 'value'=> $language])
        </form>
    </div>
@endsection
